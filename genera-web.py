#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import re
import shutil
import os


def calcula_total_setmana(s):
    buit = True
    ret = 0
    for i in range(1, 7):
        if s['p' + str(i)] != '':
            buit = False
            ret += int(s['p' + str(i)])
    return str(ret) if not buit else ''


def calcula_total_tot(s):
    buit = True
    ret = 0
    for i in range(1, 8):
        if s[i] != '':
            buit = False
            ret += int(s[i])
    return str(ret) if not buit else ''


def calcula_totals_equip(e):
    e['p'] = {}
    for i in range(1, 8):
        e['p'][i] = calcula_total_setmana(e['puntuacions']['l' + str(i)])

    e['p']['total'] = calcula_total_tot(e['p'])


def clau_sort_puntuacio(e):
    if e['p']['total'] == '':
        return str(9999) + e['id']
    else:
        return str(999 - int(e['p']['total'])).zfill(4) + e['id']


def clau_sort_alfabetic(e):
    return e['id']


def termini(i):
    if (ll[i+2]):
        return 'termini tancat'
    else:
        dia = str(i + 4) + ' de maig'
        return 'termini: ' + dia + ' a les 12:00h'


def resolucions(i):
    ret = ''
    if (ll[i+2]):
        for p in res[i]:
            if ret == '':
                ret += ', resolucions: '
            else:
                ret += ', '
            ret += '<a href="resolucions/2021/llista-' + str(i) + '/p' + p[1] + '.pdf">P' + p[1] + '</a>'

    return ret


def crea_links_llistes():
    llistes = ''    # Llistes de problemes
    for i in range(1, 8):
        if ll[i]:
            if (i != 1):
                llistes += '\n        '
            llistes += '<li><a href="llistes/2021/llista-' + str(i) + '.pdf">Llista ' + str(i) + '</a> (' + termini(i) + ')' + resolucions(i) + '</li>'

    return llistes


def calcula_dades_equips():
    for e in d['equips']:
        e['id'] = e['nom'].lower().replace(' ', '-').replace('\'', '-')
        calcula_totals_equip(e)


def trofeu(e, n):
    color = ''
    if (e['nom'] == 'Francesco Virgolini'):
        color = '#ffd700'
        n -= 1
    elif (n == 2):
        color = '#c0c0c0'
    elif (n == 3):
        color = '#cd7f32'

    if color != '':
        return (n, '<svg style="width:17px;fill:' + color + '" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M552 64H448V24c0-13.3-10.7-24-24-24H152c-13.3 0-24 10.7-24 24v40H24C10.7 64 0 74.7 0 88v56c0 35.7 22.5 72.4 61.9 100.7 31.5 22.7 69.8 37.1 110 41.7C203.3 338.5 240 360 240 360v72h-48c-35.3 0-64 20.7-64 56v12c0 6.6 5.4 12 12 12h296c6.6 0 12-5.4 12-12v-12c0-35.3-28.7-56-64-56h-48v-72s36.7-21.5 68.1-73.6c40.3-4.6 78.6-19 110-41.7 39.3-28.3 61.9-65 61.9-100.7V88c0-13.3-10.7-24-24-24zM99.3 192.8C74.9 175.2 64 155.6 64 144v-16h64.2c1 32.6 5.8 61.2 12.8 86.2-15.1-5.2-29.2-12.4-41.7-21.4zM512 144c0 16.1-17.7 36.1-35.3 48.8-12.5 9-26.7 16.2-41.8 21.4 7-25 11.8-53.6 12.8-86.2H512v16z"/></svg> ')
    else:
        return (n, '')

def crea_files_puntuacions():
    d['equips'].sort(key=clau_sort_puntuacio)
    fp = ''         # Files de la taula de puntuacions
    primer = True
    comptador = 1
    for e in d['equips']:
        comptador += 1
        if primer:
            primer = False
        else:
            fp += '\n          '

        (comptador, s) = trofeu(e, comptador)
        fp += '<tr>\n'
        fp += '            <td><a href="#' + e['id'] + '">' + e['nom'] + '</a></td>\n' # +s+ por ahi
        fp += '            <td><strong>' + e['p']['total'] + '</strong></td>\n'
        for i in range(1, 8):
            fp += '            <td>' + e['p'][i] + '</td>\n'
        fp += '          </tr>'

    return fp


def crea_informacio_equips():
    d['equips'].sort(key=clau_sort_alfabetic)
    primer = True
    ie = ''
    for e in d['equips']:
        if primer:
            primer = False
        else:
            ie += '\n      '
        ie += '<strong id="' + e['id'] + '">' + e['nom'] + '</strong>\n'
        ie += '      <ul>\n'
        ie += '        <li>Integrants: ' + " i ".join([", ".join(e['integrants'][:-1]),e['integrants'][-1]]) + '.</li>\n'
        ie += '        <li>Puntuacions per problema</li>\n'
        ie += '        <div class="contenidor-taula">\n'
        ie += '          <table>\n'
        ie += '            <tr>\n'
        ie += '              <th></th>\n'
        ie += '              <th>Total</th>\n'
        ie += '              <th>P1</th>\n'
        ie += '              <th>P2</th>\n'
        ie += '              <th>P3</th>\n'
        ie += '              <th>P4</th>\n'
        ie += '              <th>P5</th>\n'
        ie += '              <th>P6</th>\n'
        ie += '            </tr>\n'

        for i in range(1, 8):
            ie += '            <tr>\n'
            ie += '              <th>Llista ' + str(i) + '</th>\n'
            ie += '              <td><strong>' + e['p'][i] + '</strong></td>\n'
            for j in range(1, 7):
                ie += '              <td>' + e['puntuacions']['l' + str(i)]['p' + str(j)] + '</td>\n'
            ie += '            </tr>\n'
        ie += '          </table>\n'
        ie += '        </div>\n'
        ie += '      </ul>'

    return ie



with open('dades-2021.json', 'r') as f:
    d = json.load(f)

with open('index-2021.html', 'r') as f:
    html = f.read()

try:
    os.mkdir('public')
except:
    shutil.rmtree('public')
    os.mkdir('public')

os.mkdir('public/llistes')
os.mkdir('public/llistes/2021')
os.mkdir('public/resolucions')
os.mkdir('public/resolucions/2021')

# Té llargada més gran per a indexar-ho sobre 1 hahaha
# L'he feta encara més gran per a que no falli al fer i+2
ll = [False] * 10
# ll[8] = True      # final termini llista 6
# ll[9] = True      # final termini llista 7

for i in range(1, 8):
    if os.path.isfile('llistes/2021/llista-' + str(i) + '.pdf'):
        shutil.copyfile('llistes/2021/llista-' + str(i) + '.pdf', 'public/llistes/2021/llista-' + str(i) + '.pdf')
        ll[i] = True

# Indexar sobre 1...
res = [[], [], [], [], [], [], [], []]
if os.path.isdir('resolucions'):
    for i in range(1, 8):
        if os.path.isdir('resolucions/2021/llista-' + str(i)):
            primer = True
            for fitxer in os.listdir('resolucions/2021/llista-' + str(i)):
                if primer:
                    primer = False
                    os.mkdir('public/resolucions/2021/llista-' + str(i))
                r = re.search('p([1-6])-([0-9]+)\.pdf', fitxer)
                res[i].append([r.group(1), r.group(2)])
                sortida = 'public/resolucions/2021/llista-' + str(i) + '/' + 'p' + r.group(2) + '.pdf'
                shutil.copyfile('resolucions/2021/llista-' + str(i) + '/' + fitxer , sortida)
            res[i].sort(key=lambda x : int(x[0]))

calcula_dades_equips()

html = re.sub('<!-- comentari(.|\n)*?/comentari -->', '', html)
html = re.sub('/\* comentari(.|\n)*?/comentari \*/', '', html)
html = re.sub('<!-- llistes -->', crea_links_llistes(), html)
html = re.sub('<!-- puntuacions-equips -->', crea_files_puntuacions(), html)
html = re.sub('<!-- informacio-equips -->', crea_informacio_equips(), html)

with open('public/index.html', 'w') as f:
    f.write(html)
